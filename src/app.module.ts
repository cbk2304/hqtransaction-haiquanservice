import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { HaiquanModule } from './haiquan/haiquan.module';
import configuration from './haiquan/config/configuration';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [configuration],
    }),
    HaiquanModule,
  ],
})
export class AppModule {}
