import { Module } from '@nestjs/common';
import { HaiquanService } from './haiquan.service';
import { HaiquanController } from './haiquan.controller';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { SendRes } from './common/SendRes';

@Module({
  imports: [
    ConfigModule.forRoot(),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) =>
        configService.get('jwt'),
      inject: [ConfigService],
    }),
  ],

  controllers: [HaiquanController],
  providers: [HaiquanService,SendRes]
})
export class HaiquanModule {}
