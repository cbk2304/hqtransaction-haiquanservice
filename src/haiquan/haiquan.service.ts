import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { SendRes } from 'src/haiquan/common/SendRes';
import {
  ReqGetProcessingStatusDto,
  ReqGetUidDetailInfoDto,
  ReqProcessingRequestDto,
  ReqTokenDto,
  ReqUpdateBankProcessStatusDto,
} from './dto/Req.dto';
import { ConfigService } from '@nestjs/config';
import {
  ResGetProcessingStatusData,
  ResGetUidDetailInfoData,
  ResProcessingRequestData,
  ResTokenData,
} from './Type/Type';

@Injectable()
export class HaiquanService {
  constructor(
    private jwtService: JwtService,
    private configService: ConfigService,
    private readonly sendRes: SendRes,
  ) {}
  reqToken(reqTokenDto: ReqTokenDto) {
    const payload = {
      username: reqTokenDto.Data.username,
      password: reqTokenDto.Data.password,
    };
    const expiresIn =
      this.configService.get('jwt').signOptions.expiresIn || 2304;
    const token = this.jwtService.sign(payload);
    const data: ResTokenData = {
      Token: token,
      ExpiresIn: expiresIn,
    };
    const messtype: string = '156';
    return this.sendRes.ReplyReq(reqTokenDto, data);
  }

  reqPR(reqTokenDto: ReqProcessingRequestDto) {
    const data: ResProcessingRequestData = {
      MessageResp301: 'heloo',
    };
    const messtype: string = '252';
    return this.sendRes.ReplyReq(reqTokenDto, data);
  }
  reqUBPS(reqUBPSDto: ReqUpdateBankProcessStatusDto) {
    const data: string = '';
    const messtype: string = '253';
    return this.sendRes.ReplyReq(reqUBPSDto, data);
  }
  reqGUDI(reqGUDIDto: ReqGetUidDetailInfoDto) {
    const data: ResGetUidDetailInfoData = {
      DS_UID: [
        {
          UID: 'string',
          Ngay_Tao: 'string',
          So_TK: 'string',
          Nam_DK: 2022,
          Ngay_DK: 'string',
          Loai_Thue: 'string',
          TTNo: 9999,
          Ten_TTN: 'string',
          Ma_Cuc: 'string',
          Ten_Cuc: 'string',
          Ma_HQ_PH: 99000,
          Ten_HQ_PH: 'string',
          Ma_HQ_CQT: 'string',
          Ma_DV: 'string',
          Ten_DV: 'string',
          Ma_Chuong: 2,
          Ma_HQ: 'string',
          Ten_HQ: 'string',
          Ma_LH: 'string',
          Ten_LH: 'string',
          Ma_NTK: 222,
          Ten_NTK: 'string',
          Ma_LT: 333,
          Ma_HTVCHH: 123,
          Ten_HTVCHH: 'string',
          Ma_KB: 'string',
          Ten_KB: 'string',
          TKKB: 'string',
          TTNo_CT: 567,
          Ten_TTN_VT: 'string',
          Ma_NH_PH: 'string',
          Ten_NH_PH: 'string',
          Ma_NH_TH: 'string',
          Ten_NH_TH: 'string',
          KyHieu_CT: 'string',
          So_CT: 'string',
          Ngay_BN: 'string',
          Ngay_BC: 'string',
          Ngay_CT: 'string',
          Ma_NT: 'string',
          Ty_Gia: 2.5,
          SoTIen_TO: 123,
          Trang_Thai: 234,
        },
      ],
      DS_STHUE: [
        {
          LoaiThue: 'string',
          Khoan: 'string',
          TieuMuc: 444,
          DuNo: 555,
        },
        {
          LoaiThue: 'string',
          Khoan: 'string',
          TieuMuc: 22,
          DuNo: 333,
        },
      ],
      TT_TKKB: {
        Ma_NH: 'string',
        Ten_NH: 'string',
      },
    };
    const messtype: string = '254';
    return this.sendRes.ReplyReq(reqGUDIDto, data);
  }
  reqGPS(reqGPSDto: ReqGetProcessingStatusDto) {
    const data: ResGetProcessingStatusData = {
      So_TK: 'string',
      Ngay_DK: 'string',
      Ma_HQ: 'string',
      Ten_HQ: 'string',
      Ma_LH: 'string',
      Ten_LH: 'string',
      Ma_DV: 'string',
      Ten_DV: 'string',
      Ma_NH: 'string',
      Ten_NH: 'string',
      So_TN_CT: 'string',
      Ngay_TN_CT: 'string',
      TRANG_THAI: 'string',
    };
    const messtype: string = '255';
    return this.sendRes.ReplyReq(reqGPSDto, data);
  }
}
