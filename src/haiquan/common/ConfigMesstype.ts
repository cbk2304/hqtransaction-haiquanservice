export const CONFIG_MESSTYPE = [
  {
    req: '257',
    res: '157',
  },
  {
    req: '256',
    res: '156',
  },
  {
    req: '151',
    res: '251',
  },
  {
    req: '152',
    res: '252',
  },
  {
    req: '153',
    res: '253',
  },
  {
    req: '154',
    res: '254',
  },
  {
    req: '155',
    res: '255',
  },
];
