import { HeaderReqRes, ResReqFormType } from '../Type/Type';
import { PspResDto } from '../dto/PspRes.dto';
import { CONFIG_MESSTYPE } from './ConfigMesstype';

export class SendRes {
  ReplyReq(reqtoken: PspResDto, data: any) {
    const replyheader: HeaderReqRes = reqtoken.Header;
    const date = new Date();
    replyheader.Request_ID = reqtoken.Header.Message_Type;
    replyheader.Transaction_Date = date.toLocaleDateString();
    const messType =
      CONFIG_MESSTYPE.find((e) => e.req === reqtoken.Header.Message_Type)
        ?.res || '';
    replyheader.Message_Type = messType;
    const resTokenPSP: ResReqFormType = {
      Header: replyheader,
      Data: data,
      Signature: reqtoken.Signature,
    };
    return resTokenPSP;
  }
}
