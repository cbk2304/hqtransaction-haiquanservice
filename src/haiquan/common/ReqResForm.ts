import { ResReqFormType } from '../Type/Type';

export const FormReqRes = (resData: ResReqFormType) => {
  const res = {};

  for (const key in resData) {
    res[key] = resData[key];
  }
  return res;
};
