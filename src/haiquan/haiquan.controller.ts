import {
  Controller,
  UseFilters,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import {
  ReqGetProcessingStatusDto,
  ReqGetUidDetailInfoDto,
  ReqProcessingRequestDto,
  ReqTokenDto,
  ReqUpdateBankProcessStatusDto,
} from './dto/Req.dto';
import { ValidationExceptionFilter } from './Exception/validation.exception';
import { HaiquanService } from './haiquan.service';
import { LoggingInterceptor } from './Interceptor/logging.interceptor';

@Controller()
@UseInterceptors(new LoggingInterceptor())
export class HaiquanController {
  constructor(private readonly haiquanService: HaiquanService) {}
  @UsePipes(new ValidationPipe())
  @UseFilters(new ValidationExceptionFilter())
  @MessagePattern('request-Token-Haiquan')
  reqToken(@Payload() reqTokenDto: ReqTokenDto) {
    return this.haiquanService.reqToken(reqTokenDto);
  }
  @UsePipes(new ValidationPipe())
  @UseFilters(new ValidationExceptionFilter())
  @MessagePattern('request-ProcessingRequest')
  processingRequest(@Payload() reqPRDto: ReqProcessingRequestDto) {
    return this.haiquanService.reqPR(reqPRDto);
  }
  @UsePipes(new ValidationPipe())
  @UseFilters(new ValidationExceptionFilter())
  @MessagePattern('request-GetProcessingStatus')
  getProcessingStatus(@Payload() reqGPSDto: ReqGetProcessingStatusDto) {
    return this.haiquanService.reqGPS(reqGPSDto);
  }
  @UsePipes(new ValidationPipe())
  @UseFilters(new ValidationExceptionFilter())
  @MessagePattern('request-GetUidDetailInfo')
  getUidDetailInfo(@Payload() reqGUDIDto: ReqGetUidDetailInfoDto) {
    return this.haiquanService.reqGUDI(reqGUDIDto);
  }
  @UsePipes(new ValidationPipe())
  @UseFilters(new ValidationExceptionFilter())
  @MessagePattern('request-UpdateBankProcessStatus')
  updateBankProcessStatus(
    @Payload() reqUBPSDto: ReqUpdateBankProcessStatusDto,
  ) {
    return this.haiquanService.reqUBPS(reqUBPSDto);
  }
}
