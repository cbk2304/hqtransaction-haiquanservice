import { readFileSync } from 'fs';
import * as yaml from 'js-yaml';
import * as path from 'path';

const YAML_CONFIG_FILENAME = process.env.configfile || 'config.yaml';

export default () => {
  const config = yaml.load(
    readFileSync(path.resolve(YAML_CONFIG_FILENAME), 'utf8'),
  ) as Record<string, any>;
  return config;
};
