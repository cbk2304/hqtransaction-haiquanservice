import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  Logger,
} from '@nestjs/common';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  private readonly logger = new Logger(LoggingInterceptor.name);
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const natscontext = context.getArgByIndex(1);
    this.logger.log(
      'REQUEST: ' + 'NATSCONTEXT: {' + natscontext.getArgByIndex(0) + '}',
    );

    return next.handle().pipe(
      catchError((err) => {
        this.logger.log(
          'ERROR: NATSCONTEXT: {' +
            natscontext.getArgByIndex(0) +
            '} ' +
            'MESSAGE:  ' +
            err.response.message,
        );
        return throwError(() => err);
      }),
      tap((e) => {
        this.logger.log(
          'RESPONESE: NATSCONTEXT: {' +
            natscontext.getArgByIndex(0) +
            '}  SUCCESS',
        );
      }),
    );
  }
}
