import { Type } from 'class-transformer';
import { IsObject, ValidateNested } from 'class-validator';
import { DataReqTokenDto } from './ReqTokenData.dto';
import { PspResDto } from './PspRes.dto';
import { ReqProcessingRequestDataDto } from './ReqProcessingRequestData.dto';
import { ReqGetProcessingStatusDataDto } from './ReqGetProcessingStatusData.dto';
import { ReqGetUidDetailInfoDataDto } from './ReqGetUidDetailInfoData.dto';
import { ReqUpdateBankProcessStatusDataDto } from './ReqUpdateBankProcessStatusData.dto';

export class ReqTokenDto extends PspResDto {
  @IsObject()
  @ValidateNested()
  @Type(() => DataReqTokenDto)
  Data: DataReqTokenDto;
}


export class ReqProcessingRequestDto extends PspResDto {
  @IsObject()
  @ValidateNested()
  @Type(() => ReqProcessingRequestDataDto)
  Data: ReqProcessingRequestDataDto;
}
export class ReqGetProcessingStatusDto extends PspResDto {
  @IsObject()
  @ValidateNested()
  @Type(() => ReqGetProcessingStatusDataDto)
  Data: ReqGetProcessingStatusDataDto;
}
export class ReqGetUidDetailInfoDto extends PspResDto {
  @IsObject()
  @ValidateNested()
  @Type(() => ReqGetUidDetailInfoDataDto)
  Data: ReqGetUidDetailInfoDataDto;
}
export class ReqUpdateBankProcessStatusDto extends PspResDto {
  @IsObject()
  @ValidateNested()
  @Type(() => ReqUpdateBankProcessStatusDataDto)
  Data: ReqUpdateBankProcessStatusDataDto;
}