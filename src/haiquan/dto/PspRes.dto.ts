import { Type } from 'class-transformer';
import { IsObject, ValidateNested } from 'class-validator';
import { HeaderDto } from './Header.dto';
import { SignatureDto } from './Signature.dto';

export class PspResDto {
  @IsObject()
  @ValidateNested()
  @Type(() => HeaderDto)
  Header: HeaderDto;

  Data?: any;

  Signature: SignatureDto;
}
