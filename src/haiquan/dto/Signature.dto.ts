import { Type } from 'class-transformer';
import {
  IsNotEmpty,
  IsObject,
  IsString,
  ValidateNested,
} from 'class-validator';

export class Sign {
  @IsNotEmpty()
  @IsString()
  Signature_Value: string;
  @IsNotEmpty()
  @IsString()
  Key_Info: string;
}

export class SignatureDto {
  @IsObject()
  @ValidateNested()
  @Type(() => Sign)
  Sign: Sign;
  @IsObject()
  @ValidateNested()
  @Type(() => Sign)
  SignApp?: Sign;
}
