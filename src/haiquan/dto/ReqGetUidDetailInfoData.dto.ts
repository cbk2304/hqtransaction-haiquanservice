import { IsNotEmpty, IsString } from 'class-validator';

export class ReqGetUidDetailInfoDataDto {
  @IsString()
  @IsNotEmpty()
  Ma_DV: string;
  @IsString()
  @IsNotEmpty()
  UID: string;
  @IsString()
  @IsNotEmpty()
  So_TK: string;
  @IsString()
  @IsNotEmpty()
  Nam_DK: string;
  @IsString()
  @IsNotEmpty()
  Kieu_Tvan: string;
}
