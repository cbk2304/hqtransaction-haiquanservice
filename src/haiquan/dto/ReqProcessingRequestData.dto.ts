import { IsNotEmpty, IsString, IsNumber } from 'class-validator';

export class ReqProcessingRequestDataDto {
  @IsNotEmpty()
  @IsString()
  UID: string;
  @IsNotEmpty()
  @IsString()
  Ma_NH_PH: string;
  @IsNotEmpty()
  @IsString()
  Ten_NH_PH: string;
  @IsNotEmpty()
  @IsString()
  Ma_NH_TH: string;
  @IsNotEmpty()
  @IsString()
  Ten_NH_TH: string;
  @IsNotEmpty()
  @IsString()
  KyHieu_CT: string;
  @IsNotEmpty()
  @IsString()
  So_CT: string;
  @IsNotEmpty()
  @IsString()
  Ngay_BN: string;
  @IsNotEmpty()
  @IsString()
  Ngay_BC: string;
  @IsNotEmpty()
  @IsString()
  Ngay_CT: string;
  @IsNotEmpty()
  @IsString()
  Ma_NT: string;
  @IsNotEmpty()
  @IsNumber()
  Ty_Gia: number;
  @IsNotEmpty()
  @IsNumber()
  SoTIen_TO: number;
  @IsNotEmpty()
  @IsString()
  Thoigian_DG: string;
  @IsNotEmpty()
  @IsString()
  Message301: string;
}
