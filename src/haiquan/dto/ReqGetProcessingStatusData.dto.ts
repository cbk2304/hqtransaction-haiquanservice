import { IsNotEmpty, IsString } from 'class-validator';

export class ReqGetProcessingStatusDataDto {
  @IsString()
  @IsNotEmpty()
  Ma_DV: string;
  @IsString()
  @IsNotEmpty()
  UID: string;
}
