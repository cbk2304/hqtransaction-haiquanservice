import { IsNotEmpty, IsString } from 'class-validator';

export class ReqUpdateBankProcessStatusDataDto {
  @IsString()
  @IsNotEmpty()
  Ma_DV: string;
  @IsString()
  @IsNotEmpty()
  UID: string;
  @IsString()
  @IsNotEmpty()
  Trang_Thai: string;
}
