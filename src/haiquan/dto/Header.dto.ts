import { IsNotEmpty, IsString } from 'class-validator';

export class HeaderDto {
  @IsNotEmpty()
  @IsString()
  Application_Name: string;
  @IsNotEmpty()
  @IsString()
  Application_Version: string;
  @IsNotEmpty()
  @IsString()
  Sender_Code: string;
  @IsNotEmpty()
  @IsString()
  Sender_Name: string;
  @IsNotEmpty()
  @IsString()
  Message_Version: string;
  @IsNotEmpty()
  @IsString()
  Message_Type: string;
  @IsNotEmpty()
  @IsString()
  Transaction_Date: string;
  @IsNotEmpty()
  @IsString()
  Transaction_ID: string;
  @IsString()
  Request_ID: string;
}
