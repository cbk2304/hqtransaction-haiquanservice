import {
  Catch,
  RpcExceptionFilter,
  ArgumentsHost,
  BadRequestException,
} from '@nestjs/common';
import { Observable, throwError } from 'rxjs';
import { RpcException } from '@nestjs/microservices';
import { ERROR_CODE } from '../common/ErrorConfig';
import { CONFIG_MESSTYPE } from '../common/ConfigMesstype';

@Catch(BadRequestException)
export class ValidationExceptionFilter
  implements RpcExceptionFilter<RpcException>
{
  catch(exception: any, host: ArgumentsHost): Observable<any> {
    const ctx = host.switchToHttp();
    const reqBody = ctx['args'][0];
    reqBody.Header.Request_ID = reqBody.Header.Message_Type;
    const messType =
      CONFIG_MESSTYPE.find((e) => e.req === reqBody.Header.Message_Type)?.res ||
      '';
    reqBody.Header.Message_Type = messType;
    const replyException: any = {
      Header: reqBody.Header,
      Data: {},
      Error: ERROR_CODE.ERROR_BAD_REQ,
      Signature: reqBody.Signature,
      PropertiesException: {
        Name: exception.name,
        Message: exception.response.message,
      },
    };

    return throwError(() => new BadRequestException(replyException));
  }
}
