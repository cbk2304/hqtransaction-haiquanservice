import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = app.get<ConfigService>(ConfigService);
  await app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.NATS,
    options: config.get('nats.options'),
  });
  await app.startAllMicroservices();
  app.listen(config.get('port'), () => {
    console.log(new Date(), `start listen on port ${config.get('port')}`);
  });
}
bootstrap();
