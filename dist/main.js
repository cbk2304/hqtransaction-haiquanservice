"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("@nestjs/config");
const core_1 = require("@nestjs/core");
const microservices_1 = require("@nestjs/microservices");
const app_module_1 = require("./app.module");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    const config = app.get(config_1.ConfigService);
    await app.connectMicroservice({
        transport: microservices_1.Transport.NATS,
        options: config.get('nats.options'),
    });
    await app.startAllMicroservices();
    app.listen(config.get('port'), () => {
        console.log(new Date(), `start listen on port ${config.get('port')}`);
    });
}
bootstrap();
//# sourceMappingURL=main.js.map