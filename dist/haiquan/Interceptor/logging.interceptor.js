"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var LoggingInterceptor_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoggingInterceptor = void 0;
const common_1 = require("@nestjs/common");
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
let LoggingInterceptor = LoggingInterceptor_1 = class LoggingInterceptor {
    constructor() {
        this.logger = new common_1.Logger(LoggingInterceptor_1.name);
    }
    intercept(context, next) {
        const natscontext = context.getArgByIndex(1);
        this.logger.log('REQUEST: ' + 'NATSCONTEXT: {' + natscontext.getArgByIndex(0) + '}');
        return next.handle().pipe((0, operators_1.catchError)((err) => {
            this.logger.log('ERROR: NATSCONTEXT: {' +
                natscontext.getArgByIndex(0) +
                '} ' +
                'MESSAGE:  ' +
                err.response.message);
            return (0, rxjs_1.throwError)(() => err);
        }), (0, operators_1.tap)((e) => {
            this.logger.log('RESPONESE: NATSCONTEXT: {' +
                natscontext.getArgByIndex(0) +
                '}  SUCCESS');
        }));
    }
};
LoggingInterceptor = LoggingInterceptor_1 = __decorate([
    (0, common_1.Injectable)()
], LoggingInterceptor);
exports.LoggingInterceptor = LoggingInterceptor;
//# sourceMappingURL=logging.interceptor.js.map