"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.STHUE = exports.TT_TKKB = exports.UID = exports.ReqUpdateBankProcessStatusData = exports.ResGetUidDetailInfoData = exports.ReqGetUidDetailInfoData = exports.ResGetProcessingStatusData = exports.ReqGetProcessingStatusData = exports.ResProcessingRequestData = exports.ReqProcessingRequestData = exports.SignForm = exports.ReqTokendata = exports.ResReqFormType = exports.ResTokenData = exports.HeaderReqRes = void 0;
class HeaderReqRes {
}
exports.HeaderReqRes = HeaderReqRes;
class ResTokenData {
}
exports.ResTokenData = ResTokenData;
class ResReqFormType {
}
exports.ResReqFormType = ResReqFormType;
class ReqTokendata {
}
exports.ReqTokendata = ReqTokendata;
class SignForm {
}
exports.SignForm = SignForm;
class ReqProcessingRequestData {
}
exports.ReqProcessingRequestData = ReqProcessingRequestData;
class ResProcessingRequestData {
}
exports.ResProcessingRequestData = ResProcessingRequestData;
class ReqGetProcessingStatusData {
}
exports.ReqGetProcessingStatusData = ReqGetProcessingStatusData;
class ResGetProcessingStatusData {
}
exports.ResGetProcessingStatusData = ResGetProcessingStatusData;
class ReqGetUidDetailInfoData {
}
exports.ReqGetUidDetailInfoData = ReqGetUidDetailInfoData;
class ResGetUidDetailInfoData {
}
exports.ResGetUidDetailInfoData = ResGetUidDetailInfoData;
class ReqUpdateBankProcessStatusData {
}
exports.ReqUpdateBankProcessStatusData = ReqUpdateBankProcessStatusData;
class UID {
}
exports.UID = UID;
class TT_TKKB {
}
exports.TT_TKKB = TT_TKKB;
class STHUE {
}
exports.STHUE = STHUE;
//# sourceMappingURL=Type.js.map