export declare class HeaderReqRes {
    Application_Name: string;
    Application_Version: string;
    Sender_Code: string;
    Sender_Name: string;
    Message_Version: string;
    Message_Type: string;
    Transaction_Date: string;
    Transaction_ID: string;
    Request_ID: string;
}
export declare class ResTokenData {
    Token: string;
    ExpiresIn: string;
}
export declare class ResReqFormType {
    Header: HeaderReqRes;
    Data?: object;
    Error?: object;
    Signature: object;
}
export declare class ReqTokendata {
    username: string;
    password: string;
}
export declare class SignForm {
    Signature_Value: string;
    Key_Info: string;
}
export declare class ReqProcessingRequestData {
    UID: string;
    Ma_NH_PH: string;
    Ten_NH_PH: string;
    Ma_NH_TH: string;
    Ten_NH_TH: string;
    KyHieu_CT: string;
    So_CT: string;
    Ngay_BN: string;
    Ngay_BC: string;
    Ngay_CT: string;
    Ma_NT: string;
    Ty_Gia: number;
    SoTIen_TO: number;
    Thoigian_DG: string;
    Message301: string;
}
export declare class ResProcessingRequestData {
    MessageResp301: string;
}
export declare class ReqGetProcessingStatusData {
    Ma_DV: string;
    UID: string;
}
export declare class ResGetProcessingStatusData {
    So_TK: string;
    Ngay_DK: string;
    Ma_HQ: string;
    Ten_HQ: string;
    Ma_LH: string;
    Ten_LH: string;
    Ma_DV: string;
    Ten_DV: string;
    Ma_NH: string;
    Ten_NH: string;
    So_TN_CT: string;
    Ngay_TN_CT: string;
    TRANG_THAI: string;
}
export declare class ReqGetUidDetailInfoData {
    Ma_DV: string;
    UID: string;
    So_TK: string;
    Nam_DK: string;
    Kieu_Tvan: string;
}
export declare class ResGetUidDetailInfoData {
    DS_UID: Array<UID>;
    DS_STHUE: Array<STHUE>;
    TT_TKKB: TT_TKKB;
}
export declare class ReqUpdateBankProcessStatusData {
    Ma_DV: string;
    UID: string;
    Trang_Thai: string;
}
export declare class UID {
    UID: string;
    Ngay_Tao: string;
    So_TK: string;
    Nam_DK: number;
    Ngay_DK: string;
    Loai_Thue: string;
    TTNo: number;
    Ten_TTN: string;
    Ma_Cuc: string;
    Ten_Cuc: string;
    Ma_HQ_PH: number;
    Ten_HQ_PH: string;
    Ma_HQ_CQT: string;
    Ma_DV: string;
    Ten_DV: string;
    Ma_Chuong: number;
    Ma_HQ: string;
    Ten_HQ: string;
    Ma_LH: string;
    Ten_LH: string;
    Ma_NTK: number;
    Ten_NTK: string;
    Ma_LT: number;
    Ma_HTVCHH: number;
    Ten_HTVCHH: string;
    Ma_KB: string;
    Ten_KB: string;
    TKKB: string;
    TTNo_CT: number;
    Ten_TTN_VT: string;
    Ma_NH_PH: string;
    Ten_NH_PH: string;
    Ma_NH_TH: string;
    Ten_NH_TH: string;
    KyHieu_CT: string;
    So_CT: string;
    Ngay_BN: string;
    Ngay_BC: string;
    Ngay_CT: string;
    Ma_NT: string;
    Ty_Gia: number;
    SoTIen_TO: number;
    Trang_Thai: number;
}
export declare class TT_TKKB {
    Ma_NH: string;
    Ten_NH: string;
}
export declare class STHUE {
    LoaiThue: string;
    Khoan: string;
    TieuMuc: number;
    DuNo: number;
}
