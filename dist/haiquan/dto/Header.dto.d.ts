export declare class HeaderDto {
    Application_Name: string;
    Application_Version: string;
    Sender_Code: string;
    Sender_Name: string;
    Message_Version: string;
    Message_Type: string;
    Transaction_Date: string;
    Transaction_ID: string;
    Request_ID: string;
}
