export declare class ReqProcessingRequestDataDto {
    UID: string;
    Ma_NH_PH: string;
    Ten_NH_PH: string;
    Ma_NH_TH: string;
    Ten_NH_TH: string;
    KyHieu_CT: string;
    So_CT: string;
    Ngay_BN: string;
    Ngay_BC: string;
    Ngay_CT: string;
    Ma_NT: string;
    Ty_Gia: number;
    SoTIen_TO: number;
    Thoigian_DG: string;
    Message301: string;
}
