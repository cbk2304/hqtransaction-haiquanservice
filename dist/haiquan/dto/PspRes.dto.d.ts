import { HeaderDto } from './Header.dto';
import { SignatureDto } from './Signature.dto';
export declare class PspResDto {
    Header: HeaderDto;
    Data?: any;
    Signature: SignatureDto;
}
