export declare class STHUEDto {
    LoaiThue: string;
    Khoan: string;
    TieuMuc: number;
    DuNo: number;
}
