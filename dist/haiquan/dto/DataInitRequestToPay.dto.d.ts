import { STHUEDto } from './STHUE.dto';
import { UIDDto } from './UDI.dto';
export declare class DataReqInitRequestToPayDto {
    DS_UID: Array<UIDDto>;
    DS_STHUE: Array<STHUEDto>;
}
