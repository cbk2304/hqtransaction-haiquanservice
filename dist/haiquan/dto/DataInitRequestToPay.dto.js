"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataReqInitRequestToPayDto = void 0;
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const STHUE_dto_1 = require("./STHUE.dto");
const UDI_dto_1 = require("./UDI.dto");
class DataReqInitRequestToPayDto {
}
__decorate([
    (0, class_validator_1.IsArray)(),
    (0, class_validator_1.ValidateNested)(),
    (0, class_transformer_1.Type)(() => UDI_dto_1.UIDDto),
    __metadata("design:type", Array)
], DataReqInitRequestToPayDto.prototype, "DS_UID", void 0);
__decorate([
    (0, class_validator_1.IsArray)(),
    (0, class_validator_1.ValidateNested)(),
    (0, class_transformer_1.Type)(() => STHUE_dto_1.STHUEDto),
    __metadata("design:type", Array)
], DataReqInitRequestToPayDto.prototype, "DS_STHUE", void 0);
exports.DataReqInitRequestToPayDto = DataReqInitRequestToPayDto;
//# sourceMappingURL=DataInitRequestToPay.dto.js.map