import { DataReqTokenDto } from './ReqTokenData.dto';
import { PspResDto } from './PspRes.dto';
import { ReqProcessingRequestDataDto } from './ReqProcessingRequestData.dto';
import { ReqGetProcessingStatusDataDto } from './ReqGetProcessingStatusData.dto';
import { ReqGetUidDetailInfoDataDto } from './ReqGetUidDetailInfoData.dto';
import { ReqUpdateBankProcessStatusDataDto } from './ReqUpdateBankProcessStatusData.dto';
export declare class ReqTokenDto extends PspResDto {
    Data: DataReqTokenDto;
}
export declare class ReqProcessingRequestDto extends PspResDto {
    Data: ReqProcessingRequestDataDto;
}
export declare class ReqGetProcessingStatusDto extends PspResDto {
    Data: ReqGetProcessingStatusDataDto;
}
export declare class ReqGetUidDetailInfoDto extends PspResDto {
    Data: ReqGetUidDetailInfoDataDto;
}
export declare class ReqUpdateBankProcessStatusDto extends PspResDto {
    Data: ReqUpdateBankProcessStatusDataDto;
}
