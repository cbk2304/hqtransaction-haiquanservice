export declare class Sign {
    Signature_Value: string;
    Key_Info: string;
}
export declare class SignatureDto {
    Sign: Sign;
    SignApp?: Sign;
}
