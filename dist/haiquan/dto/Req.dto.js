"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReqUpdateBankProcessStatusDto = exports.ReqGetUidDetailInfoDto = exports.ReqGetProcessingStatusDto = exports.ReqProcessingRequestDto = exports.ReqTokenDto = void 0;
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const ReqTokenData_dto_1 = require("./ReqTokenData.dto");
const PspRes_dto_1 = require("./PspRes.dto");
const ReqProcessingRequestData_dto_1 = require("./ReqProcessingRequestData.dto");
const ReqGetProcessingStatusData_dto_1 = require("./ReqGetProcessingStatusData.dto");
const ReqGetUidDetailInfoData_dto_1 = require("./ReqGetUidDetailInfoData.dto");
const ReqUpdateBankProcessStatusData_dto_1 = require("./ReqUpdateBankProcessStatusData.dto");
class ReqTokenDto extends PspRes_dto_1.PspResDto {
}
__decorate([
    (0, class_validator_1.IsObject)(),
    (0, class_validator_1.ValidateNested)(),
    (0, class_transformer_1.Type)(() => ReqTokenData_dto_1.DataReqTokenDto),
    __metadata("design:type", ReqTokenData_dto_1.DataReqTokenDto)
], ReqTokenDto.prototype, "Data", void 0);
exports.ReqTokenDto = ReqTokenDto;
class ReqProcessingRequestDto extends PspRes_dto_1.PspResDto {
}
__decorate([
    (0, class_validator_1.IsObject)(),
    (0, class_validator_1.ValidateNested)(),
    (0, class_transformer_1.Type)(() => ReqProcessingRequestData_dto_1.ReqProcessingRequestDataDto),
    __metadata("design:type", ReqProcessingRequestData_dto_1.ReqProcessingRequestDataDto)
], ReqProcessingRequestDto.prototype, "Data", void 0);
exports.ReqProcessingRequestDto = ReqProcessingRequestDto;
class ReqGetProcessingStatusDto extends PspRes_dto_1.PspResDto {
}
__decorate([
    (0, class_validator_1.IsObject)(),
    (0, class_validator_1.ValidateNested)(),
    (0, class_transformer_1.Type)(() => ReqGetProcessingStatusData_dto_1.ReqGetProcessingStatusDataDto),
    __metadata("design:type", ReqGetProcessingStatusData_dto_1.ReqGetProcessingStatusDataDto)
], ReqGetProcessingStatusDto.prototype, "Data", void 0);
exports.ReqGetProcessingStatusDto = ReqGetProcessingStatusDto;
class ReqGetUidDetailInfoDto extends PspRes_dto_1.PspResDto {
}
__decorate([
    (0, class_validator_1.IsObject)(),
    (0, class_validator_1.ValidateNested)(),
    (0, class_transformer_1.Type)(() => ReqGetUidDetailInfoData_dto_1.ReqGetUidDetailInfoDataDto),
    __metadata("design:type", ReqGetUidDetailInfoData_dto_1.ReqGetUidDetailInfoDataDto)
], ReqGetUidDetailInfoDto.prototype, "Data", void 0);
exports.ReqGetUidDetailInfoDto = ReqGetUidDetailInfoDto;
class ReqUpdateBankProcessStatusDto extends PspRes_dto_1.PspResDto {
}
__decorate([
    (0, class_validator_1.IsObject)(),
    (0, class_validator_1.ValidateNested)(),
    (0, class_transformer_1.Type)(() => ReqUpdateBankProcessStatusData_dto_1.ReqUpdateBankProcessStatusDataDto),
    __metadata("design:type", ReqUpdateBankProcessStatusData_dto_1.ReqUpdateBankProcessStatusDataDto)
], ReqUpdateBankProcessStatusDto.prototype, "Data", void 0);
exports.ReqUpdateBankProcessStatusDto = ReqUpdateBankProcessStatusDto;
//# sourceMappingURL=Req.dto.js.map