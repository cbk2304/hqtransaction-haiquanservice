"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ValidationExceptionFilter = void 0;
const common_1 = require("@nestjs/common");
const rxjs_1 = require("rxjs");
const ErrorConfig_1 = require("../common/ErrorConfig");
const ConfigMesstype_1 = require("../common/ConfigMesstype");
let ValidationExceptionFilter = class ValidationExceptionFilter {
    catch(exception, host) {
        var _a;
        const ctx = host.switchToHttp();
        const reqBody = ctx['args'][0];
        reqBody.Header.Request_ID = reqBody.Header.Message_Type;
        const messType = ((_a = ConfigMesstype_1.CONFIG_MESSTYPE.find((e) => e.req === reqBody.Header.Message_Type)) === null || _a === void 0 ? void 0 : _a.res) ||
            '';
        reqBody.Header.Message_Type = messType;
        const replyException = {
            Header: reqBody.Header,
            Data: {},
            Error: ErrorConfig_1.ERROR_CODE.ERROR_BAD_REQ,
            Signature: reqBody.Signature,
            PropertiesException: {
                Name: exception.name,
                Message: exception.response.message,
            },
        };
        return (0, rxjs_1.throwError)(() => new common_1.BadRequestException(replyException));
    }
};
ValidationExceptionFilter = __decorate([
    (0, common_1.Catch)(common_1.BadRequestException)
], ValidationExceptionFilter);
exports.ValidationExceptionFilter = ValidationExceptionFilter;
//# sourceMappingURL=validation.exception.js.map