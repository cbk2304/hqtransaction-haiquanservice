import { RpcExceptionFilter, ArgumentsHost } from '@nestjs/common';
import { Observable } from 'rxjs';
import { RpcException } from '@nestjs/microservices';
export declare class ValidationExceptionFilter implements RpcExceptionFilter<RpcException> {
    catch(exception: any, host: ArgumentsHost): Observable<any>;
}
