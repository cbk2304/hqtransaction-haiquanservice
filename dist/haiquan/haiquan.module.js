"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HaiquanModule = void 0;
const common_1 = require("@nestjs/common");
const haiquan_service_1 = require("./haiquan.service");
const haiquan_controller_1 = require("./haiquan.controller");
const config_1 = require("@nestjs/config");
const jwt_1 = require("@nestjs/jwt");
const SendRes_1 = require("./common/SendRes");
let HaiquanModule = class HaiquanModule {
};
HaiquanModule = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot(),
            jwt_1.JwtModule.registerAsync({
                imports: [config_1.ConfigModule],
                useFactory: async (configService) => configService.get('jwt'),
                inject: [config_1.ConfigService],
            }),
        ],
        controllers: [haiquan_controller_1.HaiquanController],
        providers: [haiquan_service_1.HaiquanService, SendRes_1.SendRes]
    })
], HaiquanModule);
exports.HaiquanModule = HaiquanModule;
//# sourceMappingURL=haiquan.module.js.map