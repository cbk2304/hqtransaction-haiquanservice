import { ReqGetProcessingStatusDto, ReqGetUidDetailInfoDto, ReqProcessingRequestDto, ReqTokenDto, ReqUpdateBankProcessStatusDto } from './dto/Req.dto';
import { HaiquanService } from './haiquan.service';
export declare class HaiquanController {
    private readonly haiquanService;
    constructor(haiquanService: HaiquanService);
    reqToken(reqTokenDto: ReqTokenDto): import("./Type/Type").ResReqFormType;
    processingRequest(reqPRDto: ReqProcessingRequestDto): import("./Type/Type").ResReqFormType;
    getProcessingStatus(reqGPSDto: ReqGetProcessingStatusDto): import("./Type/Type").ResReqFormType;
    getUidDetailInfo(reqGUDIDto: ReqGetUidDetailInfoDto): import("./Type/Type").ResReqFormType;
    updateBankProcessStatus(reqUBPSDto: ReqUpdateBankProcessStatusDto): import("./Type/Type").ResReqFormType;
}
