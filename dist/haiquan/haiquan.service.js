"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HaiquanService = void 0;
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const SendRes_1 = require("./common/SendRes");
const config_1 = require("@nestjs/config");
let HaiquanService = class HaiquanService {
    constructor(jwtService, configService, sendRes) {
        this.jwtService = jwtService;
        this.configService = configService;
        this.sendRes = sendRes;
    }
    reqToken(reqTokenDto) {
        const payload = {
            username: reqTokenDto.Data.username,
            password: reqTokenDto.Data.password,
        };
        const expiresIn = this.configService.get('jwt').signOptions.expiresIn || 2304;
        const token = this.jwtService.sign(payload);
        const data = {
            Token: token,
            ExpiresIn: expiresIn,
        };
        const messtype = '156';
        return this.sendRes.ReplyReq(reqTokenDto, data);
    }
    reqPR(reqTokenDto) {
        const data = {
            MessageResp301: 'heloo',
        };
        const messtype = '252';
        return this.sendRes.ReplyReq(reqTokenDto, data);
    }
    reqUBPS(reqUBPSDto) {
        const data = '';
        const messtype = '253';
        return this.sendRes.ReplyReq(reqUBPSDto, data);
    }
    reqGUDI(reqGUDIDto) {
        const data = {
            DS_UID: [
                {
                    UID: 'string',
                    Ngay_Tao: 'string',
                    So_TK: 'string',
                    Nam_DK: 2022,
                    Ngay_DK: 'string',
                    Loai_Thue: 'string',
                    TTNo: 9999,
                    Ten_TTN: 'string',
                    Ma_Cuc: 'string',
                    Ten_Cuc: 'string',
                    Ma_HQ_PH: 99000,
                    Ten_HQ_PH: 'string',
                    Ma_HQ_CQT: 'string',
                    Ma_DV: 'string',
                    Ten_DV: 'string',
                    Ma_Chuong: 2,
                    Ma_HQ: 'string',
                    Ten_HQ: 'string',
                    Ma_LH: 'string',
                    Ten_LH: 'string',
                    Ma_NTK: 222,
                    Ten_NTK: 'string',
                    Ma_LT: 333,
                    Ma_HTVCHH: 123,
                    Ten_HTVCHH: 'string',
                    Ma_KB: 'string',
                    Ten_KB: 'string',
                    TKKB: 'string',
                    TTNo_CT: 567,
                    Ten_TTN_VT: 'string',
                    Ma_NH_PH: 'string',
                    Ten_NH_PH: 'string',
                    Ma_NH_TH: 'string',
                    Ten_NH_TH: 'string',
                    KyHieu_CT: 'string',
                    So_CT: 'string',
                    Ngay_BN: 'string',
                    Ngay_BC: 'string',
                    Ngay_CT: 'string',
                    Ma_NT: 'string',
                    Ty_Gia: 2.5,
                    SoTIen_TO: 123,
                    Trang_Thai: 234,
                },
            ],
            DS_STHUE: [
                {
                    LoaiThue: 'string',
                    Khoan: 'string',
                    TieuMuc: 444,
                    DuNo: 555,
                },
                {
                    LoaiThue: 'string',
                    Khoan: 'string',
                    TieuMuc: 22,
                    DuNo: 333,
                },
            ],
            TT_TKKB: {
                Ma_NH: 'string',
                Ten_NH: 'string',
            },
        };
        const messtype = '254';
        return this.sendRes.ReplyReq(reqGUDIDto, data);
    }
    reqGPS(reqGPSDto) {
        const data = {
            So_TK: 'string',
            Ngay_DK: 'string',
            Ma_HQ: 'string',
            Ten_HQ: 'string',
            Ma_LH: 'string',
            Ten_LH: 'string',
            Ma_DV: 'string',
            Ten_DV: 'string',
            Ma_NH: 'string',
            Ten_NH: 'string',
            So_TN_CT: 'string',
            Ngay_TN_CT: 'string',
            TRANG_THAI: 'string',
        };
        const messtype = '255';
        return this.sendRes.ReplyReq(reqGPSDto, data);
    }
};
HaiquanService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [jwt_1.JwtService,
        config_1.ConfigService,
        SendRes_1.SendRes])
], HaiquanService);
exports.HaiquanService = HaiquanService;
//# sourceMappingURL=haiquan.service.js.map