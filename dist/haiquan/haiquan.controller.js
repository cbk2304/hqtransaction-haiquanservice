"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HaiquanController = void 0;
const common_1 = require("@nestjs/common");
const microservices_1 = require("@nestjs/microservices");
const Req_dto_1 = require("./dto/Req.dto");
const validation_exception_1 = require("./Exception/validation.exception");
const haiquan_service_1 = require("./haiquan.service");
const logging_interceptor_1 = require("./Interceptor/logging.interceptor");
let HaiquanController = class HaiquanController {
    constructor(haiquanService) {
        this.haiquanService = haiquanService;
    }
    reqToken(reqTokenDto) {
        return this.haiquanService.reqToken(reqTokenDto);
    }
    processingRequest(reqPRDto) {
        return this.haiquanService.reqPR(reqPRDto);
    }
    getProcessingStatus(reqGPSDto) {
        return this.haiquanService.reqGPS(reqGPSDto);
    }
    getUidDetailInfo(reqGUDIDto) {
        return this.haiquanService.reqGUDI(reqGUDIDto);
    }
    updateBankProcessStatus(reqUBPSDto) {
        return this.haiquanService.reqUBPS(reqUBPSDto);
    }
};
__decorate([
    (0, common_1.UsePipes)(new common_1.ValidationPipe()),
    (0, common_1.UseFilters)(new validation_exception_1.ValidationExceptionFilter()),
    (0, microservices_1.MessagePattern)('request-Token-Haiquan'),
    __param(0, (0, microservices_1.Payload)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Req_dto_1.ReqTokenDto]),
    __metadata("design:returntype", void 0)
], HaiquanController.prototype, "reqToken", null);
__decorate([
    (0, common_1.UsePipes)(new common_1.ValidationPipe()),
    (0, common_1.UseFilters)(new validation_exception_1.ValidationExceptionFilter()),
    (0, microservices_1.MessagePattern)('request-ProcessingRequest'),
    __param(0, (0, microservices_1.Payload)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Req_dto_1.ReqProcessingRequestDto]),
    __metadata("design:returntype", void 0)
], HaiquanController.prototype, "processingRequest", null);
__decorate([
    (0, common_1.UsePipes)(new common_1.ValidationPipe()),
    (0, common_1.UseFilters)(new validation_exception_1.ValidationExceptionFilter()),
    (0, microservices_1.MessagePattern)('request-GetProcessingStatus'),
    __param(0, (0, microservices_1.Payload)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Req_dto_1.ReqGetProcessingStatusDto]),
    __metadata("design:returntype", void 0)
], HaiquanController.prototype, "getProcessingStatus", null);
__decorate([
    (0, common_1.UsePipes)(new common_1.ValidationPipe()),
    (0, common_1.UseFilters)(new validation_exception_1.ValidationExceptionFilter()),
    (0, microservices_1.MessagePattern)('request-GetUidDetailInfo'),
    __param(0, (0, microservices_1.Payload)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Req_dto_1.ReqGetUidDetailInfoDto]),
    __metadata("design:returntype", void 0)
], HaiquanController.prototype, "getUidDetailInfo", null);
__decorate([
    (0, common_1.UsePipes)(new common_1.ValidationPipe()),
    (0, common_1.UseFilters)(new validation_exception_1.ValidationExceptionFilter()),
    (0, microservices_1.MessagePattern)('request-UpdateBankProcessStatus'),
    __param(0, (0, microservices_1.Payload)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Req_dto_1.ReqUpdateBankProcessStatusDto]),
    __metadata("design:returntype", void 0)
], HaiquanController.prototype, "updateBankProcessStatus", null);
HaiquanController = __decorate([
    (0, common_1.Controller)(),
    (0, common_1.UseInterceptors)(new logging_interceptor_1.LoggingInterceptor()),
    __metadata("design:paramtypes", [haiquan_service_1.HaiquanService])
], HaiquanController);
exports.HaiquanController = HaiquanController;
//# sourceMappingURL=haiquan.controller.js.map