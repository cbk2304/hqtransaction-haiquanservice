export declare const ERROR_CODE: {
    ERROR_BAD_REQ: {
        code: number;
        message: string;
    };
};
