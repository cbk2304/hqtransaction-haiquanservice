import { ResReqFormType } from '../Type/Type';
import { PspResDto } from '../dto/PspRes.dto';
export declare class SendRes {
    ReplyReq(reqtoken: PspResDto, data: any): ResReqFormType;
}
