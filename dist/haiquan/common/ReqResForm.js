"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FormReqRes = void 0;
const FormReqRes = (resData) => {
    const res = {};
    for (const key in resData) {
        res[key] = resData[key];
    }
    return res;
};
exports.FormReqRes = FormReqRes;
//# sourceMappingURL=ReqResForm.js.map