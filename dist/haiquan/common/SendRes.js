"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendRes = void 0;
const ConfigMesstype_1 = require("./ConfigMesstype");
class SendRes {
    ReplyReq(reqtoken, data) {
        var _a;
        const replyheader = reqtoken.Header;
        const date = new Date();
        replyheader.Request_ID = reqtoken.Header.Message_Type;
        replyheader.Transaction_Date = date.toLocaleDateString();
        const messType = ((_a = ConfigMesstype_1.CONFIG_MESSTYPE.find((e) => e.req === reqtoken.Header.Message_Type)) === null || _a === void 0 ? void 0 : _a.res) || '';
        replyheader.Message_Type = messType;
        const resTokenPSP = {
            Header: replyheader,
            Data: data,
            Signature: reqtoken.Signature,
        };
        return resTokenPSP;
    }
}
exports.SendRes = SendRes;
//# sourceMappingURL=SendRes.js.map