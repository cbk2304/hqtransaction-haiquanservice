import { JwtService } from '@nestjs/jwt';
import { SendRes } from 'src/haiquan/common/SendRes';
import { ReqGetProcessingStatusDto, ReqGetUidDetailInfoDto, ReqProcessingRequestDto, ReqTokenDto, ReqUpdateBankProcessStatusDto } from './dto/Req.dto';
import { ConfigService } from '@nestjs/config';
export declare class HaiquanService {
    private jwtService;
    private configService;
    private readonly sendRes;
    constructor(jwtService: JwtService, configService: ConfigService, sendRes: SendRes);
    reqToken(reqTokenDto: ReqTokenDto): import("./Type/Type").ResReqFormType;
    reqPR(reqTokenDto: ReqProcessingRequestDto): import("./Type/Type").ResReqFormType;
    reqUBPS(reqUBPSDto: ReqUpdateBankProcessStatusDto): import("./Type/Type").ResReqFormType;
    reqGUDI(reqGUDIDto: ReqGetUidDetailInfoDto): import("./Type/Type").ResReqFormType;
    reqGPS(reqGPSDto: ReqGetProcessingStatusDto): import("./Type/Type").ResReqFormType;
}
