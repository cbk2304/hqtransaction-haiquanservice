"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const yaml = require("js-yaml");
const path = require("path");
const YAML_CONFIG_FILENAME = process.env.configfile || 'config.yaml';
exports.default = () => {
    const config = yaml.load((0, fs_1.readFileSync)(path.resolve(YAML_CONFIG_FILENAME), 'utf8'));
    return config;
};
//# sourceMappingURL=configuration.js.map